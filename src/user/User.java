package user;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;
import java.text.ParseException; //prova
import java.text.SimpleDateFormat; //prova
import java.util.Date; //prova


public class User {
	
	private String Nome;
	private String Cognome;
	private int Eta;
	private Phone mainPhone;
	
	// TODO: Crea attributo codiceFiscale e tutti gli attributi inerenti
	private String CodiceFiscale;
	private String AnnodiNascita;
	private String MesediNascita;
	private String GiornodiNascita;
	private String CittaNatale;
	private String Genere;
	private String DatadiNascita;
	private Date DayBorn;
	
	
	//Attributi di azione
	private Boolean staCamminando = false;
	private Boolean LetteradiControllo = false;
	private int velocitaPasso = 0;
	
	
	
	//TODO: Creare nuovo costruttore con input e memorizzazione del codice fiscale
	//da rivedere!!!!!!!!!
	
	
	public User (String CodiceFiscale) {
		this.CodiceFiscale = CodiceFiscale;
		
	}
	

	
	//Costruttore di default
	public User () {}
	
	//Costruttore di User
	public User (String NewNome, String NewCognome) {
		this.Nome = NewNome;
		this.Cognome = NewCognome;
	}
	
	//Costruttore di User
	public User (String NewNome, String NewCognome, int NewEta) {
		this.Nome = NewNome;
		this.Cognome = NewCognome;
		this.Eta = NewEta;
	}
	
	
	
	//TODO: Creare i metodi degli attributi del codice fiscale
	//All'interno dei metodi, chiamare le funzioni di calcolo parametri CF passando codiceFiscale
	
	public void getletteradiControllo() {
		this.LetteradiControllo = LetteradiControllo;
	}
	
	public void getDatadiNascita() {
		
	
		String giorno = "";
		String mese = "";
		String anno = "";
		
		String b = CodiceFiscale.substring(9,11);
		int dayBorn = Integer.parseInt(b);
		
		if (dayBorn<32) {
			giorno += dayBorn;
		}
		else if (dayBorn>32 && dayBorn<71){
			giorno += dayBorn-40;
		}
		
		
		
		
		
		 if (CodiceFiscale.charAt(8)== 'a' || CodiceFiscale.charAt(8)== 'A') { 
		    	mese +="gennaio"+ " ";
		    }
		    else if (CodiceFiscale.charAt(8)== 'b' || CodiceFiscale.charAt(8)== 'B') {
		    	mese +="febbraio" + " ";
		    }
		    else if (CodiceFiscale.charAt(8)== 'c' || CodiceFiscale.charAt(8)== 'C') {
		    	mese +="marzo" + " ";
		    }
		    else if (CodiceFiscale.charAt(8)== 'd' || CodiceFiscale.charAt(8)== 'D') {
		    	mese +="aprile" + " ";
		    }
		    else if (CodiceFiscale.charAt(8)== 'e' || CodiceFiscale.charAt(8)== 'E') {
		    	mese +="maggio"+ " ";
		    }
		    else if (CodiceFiscale.charAt(8)== 'h' || CodiceFiscale.charAt(8)== 'H') {
		    	mese +="giugno"+ " ";
		    }
		    else if (CodiceFiscale.charAt(8)== 'l' || CodiceFiscale.charAt(8)== 'L') {
		    	mese +="luglio"+ " ";
		    }
		    else if (CodiceFiscale.charAt(8)== 'm' || CodiceFiscale.charAt(8)== 'M') {
		    	mese +="agosto"+ " ";
		    }
		    else if (CodiceFiscale.charAt(8)== 'p' || CodiceFiscale.charAt(8)== 'P') {
		    	mese+="settembre" + " ";
		    }
		    else if (CodiceFiscale.charAt(8)== 'r' || CodiceFiscale.charAt(8)== 'R') {
		    	mese +="ottobre"+ " ";
		    }
		    else if (CodiceFiscale.charAt(8)== 's' || CodiceFiscale.charAt(8)== 'S') {
		    	mese +="novembre"+ " ";
		    }
		    else if (CodiceFiscale.charAt(8)== 't' || CodiceFiscale.charAt(8)== 'T') {
		    	mese +="dicembre" + " ";
		    }
		    else {
		    	System.out.println("Codice non valido"); //voglio che a questo punto ritorni all'inizio senza interrompersi
		    }
		 
		 giorno += " " + mese ;
		 
		 String a = CodiceFiscale.substring(6,8);
			int yearBorn = Integer.parseInt(a);
			int yearCheck = yearBorn + 1900;
		    int yearNow = Calendar.getInstance().get(Calendar.YEAR);
		    
		 // controlla la data di nascita
		    int dateCheck = yearNow - 85;
		    
//			controlla se si hanno piu' di 85 anni e restituisce l'anno di nascita
	        if (yearCheck<dateCheck) {
	        	int yearYoung = yearBorn + 2000;
	        	anno += yearYoung;
	        }
	        
	        else {

	        	anno += yearCheck;
	        }
	        
	        giorno += " " + anno;
	        
	       
	        SimpleDateFormat simpledateformat = new SimpleDateFormat("dd MMMM yyyy");
	        
                try {

	            Date date = simpledateformat.parse(giorno); //born
	   
	            
	            DayBorn = date;
		        
	            System.out.println(DayBorn);
		      
		      

	        } catch (ParseException e) {
	           e.printStackTrace();
	        }
	        
	        
	}
	
	public String getNome() {
		Nome= this.CodiceFiscale.substring(3,6);
		return Nome;
	}
	public void setNome(String nome) {
		
		Nome = nome;
	
	}
	public String getCognome() {
		Cognome= CodiceFiscale.substring(0,3);
		return Cognome;
	}
	public void setCognome(String cognome) {
		Cognome = cognome;
	}
	public Phone getmMainPhone() {
		return mainPhone;
		
	}
	public void setMainPhone(Phone phone) {
		mainPhone = phone;
	}
	
	
	public int getEta() {
		return Eta;
	}
	
	public void setEta(int eta) {
		Eta = eta;
	}
	
	public String getCodiceFiscale() {
		return CodiceFiscale;
	}
	public void setCodiceFiscale(String codiceFiscale) {
		CodiceFiscale = codiceFiscale;
	}
	
	public String getAnnodiNascita() {
		String anno = " ";
		String a = CodiceFiscale.substring(6,8);
		int yearBorn = Integer.parseInt(a);
		int yearCheck = yearBorn + 1900;
	    int yearNow = Calendar.getInstance().get(Calendar.YEAR);
	    
	 // controlla la data di nascita
	    int dateCheck = yearNow - 85;
	    
//		controlla se si hanno piu' di 85 anni e restituisce l'anno di nascita
        if (yearCheck<dateCheck) {
        	int yearYoung = yearBorn + 2000;
        	anno += yearYoung;
        }
        
        else {

        	anno += yearCheck;
        }
        
        int old = yearNow - yearCheck;
        this.setEta(old);
        
        //non calcola il giorno e il mese quindi � sbagliato
        AnnodiNascita = anno; 
        
        return AnnodiNascita;
        
	    
		
	}
	public void setAnnodiNascita(String annoNascita) {
		AnnodiNascita = annoNascita;
	}
	
	public String getMesediNascita() {
		
		String mese = " ";
		
		
//	      resituisce il mese di nascita e controlla che nel codice ci siano lettere valide
        
		    if (CodiceFiscale.charAt(8)== 'a' || CodiceFiscale.charAt(8)== 'A') { 
		    	mese +="gennaio"+ " ";
		    }
		    else if (CodiceFiscale.charAt(8)== 'b' || CodiceFiscale.charAt(8)== 'B') {
		    	mese +="febbraio" + " ";
		    }
		    else if (CodiceFiscale.charAt(8)== 'c' || CodiceFiscale.charAt(8)== 'C') {
		    	mese +="marzo" + " ";
		    }
		    else if (CodiceFiscale.charAt(8)== 'd' || CodiceFiscale.charAt(8)== 'D') {
		    	mese +="aprile" + " ";
		    }
		    else if (CodiceFiscale.charAt(8)== 'e' || CodiceFiscale.charAt(8)== 'E') {
		    	mese +="maggio"+ " ";
		    }
		    else if (CodiceFiscale.charAt(8)== 'h' || CodiceFiscale.charAt(8)== 'H') {
		    	mese +="giugno"+ " ";
		    }
		    else if (CodiceFiscale.charAt(8)== 'l' || CodiceFiscale.charAt(8)== 'L') {
		    	mese +="luglio"+ " ";
		    }
		    else if (CodiceFiscale.charAt(8)== 'm' || CodiceFiscale.charAt(8)== 'M') {
		    	mese +="agosto"+ " ";
		    }
		    else if (CodiceFiscale.charAt(8)== 'p' || CodiceFiscale.charAt(8)== 'P') {
		    	mese+="settembre" + " ";
		    }
		    else if (CodiceFiscale.charAt(8)== 'r' || CodiceFiscale.charAt(8)== 'R') {
		    	mese +="ottobre"+ " ";
		    }
		    else if (CodiceFiscale.charAt(8)== 's' || CodiceFiscale.charAt(8)== 'S') {
		    	mese +="novembre"+ " ";
		    }
		    else if (CodiceFiscale.charAt(8)== 't' || CodiceFiscale.charAt(8)== 'T') {
		    	mese +="dicembre" + " ";
		    }
		    else {
		    	System.out.println("Codice non valido"); //voglio che a questo punto ritorni all'inizio senza interrompersi
		    }
		    
		    MesediNascita = mese;
		return MesediNascita;
	}
	
	
	public void setMesediNascita(String meseNascita) {
		MesediNascita = meseNascita;
	}
	
	public String getGiornodiNascita() {
		
		String giorno = " ";
		
		String b = CodiceFiscale.substring(9,11);
		int dayBorn = Integer.parseInt(b);
		
		if (dayBorn<32) {
			giorno += dayBorn;
		}
		else if (dayBorn>32 && dayBorn<71){
			giorno += dayBorn-40;
		}
		
		GiornodiNascita = giorno;
		return GiornodiNascita;
	}
	
	
	
	public void setGiornodiNascita(String giornoNascita) {
		GiornodiNascita = giornoNascita;
	}
	
	public void getCittaNatale() {
		
		String[] myArrayComuni = new String[]{"ABANO TERME","ABBADIA CERRETO","TORTONA", "NICOSIA"};

	    String[] myArrayCodici = new String[]{"A001","A004","L304", "F892"};
	    

	    
	    
	    Map<String, String> myArrayComuniMap = new HashMap<String, String>();
	    
	    
	      
	    
	    myArrayComuniMap.put("A001", "ABANO TERME");  
	    myArrayComuniMap.put("A004", "ABBADIA CERRETO");
	    myArrayComuniMap.put("L304", "TORTONA");
	    myArrayComuniMap.put("F892", "NICOSIA");
	    
	    System.out.println("La tua citta natale e':" + " " + myArrayComuniMap.get(CodiceFiscale.substring(11,15)));
	    
	  
		
	}
	public void setCittaNatale(String citta) {
		CittaNatale = citta;
	}
	
	public void getGenere() {
		
        String genere = " ";
		
		String b = CodiceFiscale.substring(9,11);
		
		int dayBorn = Integer.parseInt(b);
		
		if (dayBorn<32) {
			genere += "maschio";
			System.out.println("Il tuo genere e':" + " " + genere);
		}
		else if (dayBorn>32 && dayBorn<71){
			genere += "femmina";
			System.out.println("Il tuo genere e':" + " " + genere);
		}


		this.Genere = genere;
		
		
	}
	public void setGenere(String genere) {
		Genere = genere;
	}
	
	
	public void cammina(Boolean isCammina) {
		this.staCamminando = isCammina;
		if(isCammina) {
			 this.velocitaPasso += 1;
		} else {
			 this.velocitaPasso = 0;
		}
	}
		

	public void immettiCodice() {
	System.out.println("Digita il Codice Fiscale e premi invio:");
	Scanner codicefisc = new Scanner(System.in); //metto try catch? qui c'e' il problema su Main.Main
		CodiceFiscale = codicefisc.nextLine();
		//mettere lettera di controllo in un metodo separato
		

		//this.Controllo(CodiceFiscale);
	
		//if (true) {
		
	    this.setCodiceFiscale(CodiceFiscale);
	    
		//}
		//else {
		//	System.out.println("Lettera di controllo errata.");
		//}
	
	}
  		
	
	public boolean Controllo() {
		
//      lettera di controllo ma non funziona
		char w = '.';
		int x = this.CodiceFiscale.length();
		int sommaPari = 0;
		int sommaDisp = 0;
		
		for (int k=0; k<x; k++) {
			
//			converto tutti i caratteri in posizione pari nei numeri relativi alla tab dei pari (tranne la lettera di controllo) e li sommo tra loro
	    	if (k%2==1) {
//	    		metto tutti i caratteri pari in un array separato
	    		char carPari = this.CodiceFiscale.charAt(k);
	    		char [] caratteriPari = new char [16]; 
	    		int [] numPosPari = new int [16];
	    		
	    		
	    		caratteriPari[k] = carPari; 
//	    		System.out.println(caratteriPari[k]);
		for (int l=0; l<caratteriPari.length-1; l++) { 
	    			
	    			if (caratteriPari[l]=='a' || caratteriPari[l]=='A' ||caratteriPari[l]=='0' ) {
	    				numPosPari[l]=0;
	    				sommaPari += numPosPari[l];
	    				
	    				
	    			}
	    			else if (caratteriPari[l]=='b' || caratteriPari[l]=='B'||caratteriPari[l]=='1') {
	    				numPosPari[l]=1;
	    				sommaPari += numPosPari[l];
	    				
	    				
	    			}
	    			else if (caratteriPari[l]=='c' || caratteriPari[l]=='C'||caratteriPari[l]=='2') {
	    				numPosPari[l]=2;
	    				sommaPari += numPosPari[l];
	    				
	    			
	    			}
	    			else if (caratteriPari[l]=='d' || caratteriPari[l]=='D'||caratteriPari[l]=='3') {
	    				numPosPari[l]=3;
	    				sommaPari += numPosPari[l];
	    				
	    				
	    			}
	    			else if (caratteriPari[l]=='e' || caratteriPari[l]=='E'||caratteriPari[l]=='4') {
	    				numPosPari[l]=4;
	    				sommaPari += numPosPari[l];
	    				
	    				
	    			}
	    			else if (caratteriPari[l]=='f' || caratteriPari[l]=='F'||caratteriPari[l]=='5') {
	    				numPosPari[l]=5;
	    				sommaPari += numPosPari[l];
	    				
	    				
	    			}
	    			else if (caratteriPari[l]=='g' || caratteriPari[l]=='G'||caratteriPari[l]=='6') {
	    				numPosPari[l]=6;
	    				sommaPari += numPosPari[l];
	    				
	    				
	    			}
	    			else if (caratteriPari[l]=='h' || caratteriPari[l]=='H'||caratteriPari[l]=='7') {
	    				numPosPari[l]=7;
	    				sommaPari += numPosPari[l];
	    				
	    				
	    			}
	    			else if (caratteriPari[l]=='i' || caratteriPari[l]=='I'||caratteriPari[l]=='8') {
	    				numPosPari[l]=8;
	    				sommaPari += numPosPari[l];
	    				
	    				
	    			}
	    			else if (caratteriPari[l]=='j' || caratteriPari[l]=='J'||caratteriPari[l]=='9') {
	    				numPosPari[l]=9;
	    				sommaPari += numPosPari[l];
	    				
	    				
	    			}
	    			else if (caratteriPari[l]=='k' || caratteriPari[l]=='K') {
	    				numPosPari[l]=10;
	    				sommaPari += numPosPari[l];
	    				
	    				
	    			}
	    			else if (caratteriPari[l]=='l' || caratteriPari[l]=='L') {
	    				numPosPari[l]=11;
	    				sommaPari += numPosPari[l];
	    				
	    				
	    			}
	    			else if (caratteriPari[l]=='m' || caratteriPari[l]=='M') {
	    				numPosPari[l]=12;
	    				sommaPari += numPosPari[l];
	    				
	    				
	    			}
	    			else if (caratteriPari[l]=='n' || caratteriPari[l]=='N') {
	    				numPosPari[l]=13;
	    				sommaPari += numPosPari[l];
	    				
	    				
	    			}
	    			else if (caratteriPari[l]=='o' || caratteriPari[l]=='O') {
	    				numPosPari[l]=14;
	    				sommaPari += numPosPari[l];
	    				
	    				
	    			}
	    			else if (caratteriPari[l]=='p' || caratteriPari[l]=='P') {
	    				numPosPari[l]=15;
	    				sommaPari += numPosPari[l];
	    				
	    				
	    			}
	    			else if (caratteriPari[l]=='q' || caratteriPari[l]=='Q') {
	    				numPosPari[l]=16;
	    				sommaPari += numPosPari[l];
	    				
	    				
	    			}
	    			else if (caratteriPari[l]=='r' || caratteriPari[l]=='R') {
	    				numPosPari[l]=17;
	    				sommaPari += numPosPari[l];
	    
	    				
	    			}
	    			else if (caratteriPari[l]=='s' || caratteriPari[l]=='S') {
	    				numPosPari[l]=18;
	    				sommaPari += numPosPari[l];
	    				
	    				
	    			}
	    			else if (caratteriPari[l]=='t' || caratteriPari[l]=='T') {
	    				numPosPari[l]=19;
	    				sommaPari += numPosPari[l];
	    				
	    				
	    			}
	    			else if (caratteriPari[l]=='u' || caratteriPari[l]=='U') {
	    				numPosPari[l]=20;
	    				sommaPari += numPosPari[l];
	    				
	    				
	    			}
	    			else if (caratteriPari[l]=='v' || caratteriPari[l]=='V') {
	    				numPosPari[l]=21;
	    				sommaPari += numPosPari[l];
	    				
	    				
	    			}
	    			else if (caratteriPari[l]=='w' || caratteriPari[l]=='W') {
	    				numPosPari[l]=22;
	    				sommaPari += numPosPari[l];
	    				
	    				
	    			}
	    			else if (caratteriPari[l]=='x' || caratteriPari[l]=='X') {
	    				numPosPari[l]=23;
	    				sommaPari += numPosPari[l];
	    				
	    			
	    			}
	    			else if (caratteriPari[l]=='y' || caratteriPari[l]=='Y') {
	    				numPosPari[l]=24;
	    				sommaPari += numPosPari[l];
	    				
	    				
	    			}
	    			else if (caratteriPari[l]=='z' || caratteriPari[l]=='Z') {
	    				numPosPari[l]=25;
	    				sommaPari += numPosPari[l];
	    				
	    				
	    			}
	    			
	    		
	    	}		
	    	}
	    	
//			converto tutti i caratteri in posizione dispari nei numeri relativi alla tab dei dispari e li sommo tra loro
	    	
     	else if (k%2==0) {
	    		char carDisp = CodiceFiscale.charAt(k);
	    		char [] caratteriDisp = new char [16]; 
	    		int [] numPosDisp = new int [16];
	    		
	    		caratteriDisp[k] = carDisp; //riempio l'array
              for (int n=0; n<caratteriDisp.length; n++) { 
	    			
	    			if (caratteriDisp[n]=='a' || caratteriDisp[n]=='A' ||caratteriDisp[n]=='0' ) {
	    				numPosDisp[n]=1;
	    				sommaDisp += numPosDisp[n];
	    				
	    				
	    			}
	    			else if (caratteriDisp[n]=='b' || caratteriDisp[n]=='B' ||caratteriDisp[n]=='1' ) {
	    				numPosDisp[n]=0;
	    				sommaDisp += numPosDisp[n];
	    				
	    				
	    			}
	    			else if (caratteriDisp[n]=='c' || caratteriDisp[n]=='C' ||caratteriDisp[n]=='2' ) {
	    				numPosDisp[n]=5;
	    				sommaDisp += numPosDisp[n];
	    				
	    				
	    			}
	    			else if (caratteriDisp[n]=='d' || caratteriDisp[n]=='D' ||caratteriDisp[n]=='3' ) {
	    				numPosDisp[n]=7;
	    				sommaDisp += numPosDisp[n];
	    				
	    				
	    			}
	    			else if (caratteriDisp[n]=='e' || caratteriDisp[n]=='E' ||caratteriDisp[n]=='4' ) {
	    				numPosDisp[n]=9;
	    				sommaDisp += numPosDisp[n];
	    				
	    				
	    			}
	    			else if (caratteriDisp[n]=='f' || caratteriDisp[n]=='F' ||caratteriDisp[n]=='5' ) {
	    				numPosDisp[n]=13;
	    				sommaDisp += numPosDisp[n];
	    				
	    				
	    			}
	    			else if (caratteriDisp[n]=='g' || caratteriDisp[n]=='G' ||caratteriDisp[n]=='6' ) {
	    				numPosDisp[n]=15;
	    				sommaDisp += numPosDisp[n];
	    				
	    				
	    			}
	    			else if (caratteriDisp[n]=='h' || caratteriDisp[n]=='H' ||caratteriDisp[n]=='7' ) {
	    				numPosDisp[n]=17;
	    				sommaDisp += numPosDisp[n];
	    				
	    				
	    			}
	    			else if (caratteriDisp[n]=='i' || caratteriDisp[n]=='I' ||caratteriDisp[n]=='8' ) {
	    				numPosDisp[n]=19;
	    				sommaDisp += numPosDisp[n];
	    				
	    				
	    			}
	    			else if (caratteriDisp[n]=='j' || caratteriDisp[n]=='J' ||caratteriDisp[n]=='9' ) {
	    				numPosDisp[n]=21;
	    				sommaDisp += numPosDisp[n];
	    				
	    				
	    			}
	    			else if (caratteriDisp[n]=='k' || caratteriDisp[n]=='K') {
	    				numPosDisp[n]=2;
	    				sommaDisp += numPosDisp[n];
	    				
	    				
	    			}
	    			else if (caratteriDisp[n]=='l' || caratteriDisp[n]=='L' ) {
	    				numPosDisp[n]=4;
	    				sommaDisp += numPosDisp[n];
	    				
	    				
	    			}
	    			else if (caratteriDisp[n]=='m' || caratteriDisp[n]=='M' ) {
	    				numPosDisp[n]=18;
	    				sommaDisp += numPosDisp[n];
	    				
	    				
	    			}
	    			else if (caratteriDisp[n]=='n' || caratteriDisp[n]=='N' ) {
	    				numPosDisp[n]=20;
	    				sommaDisp += numPosDisp[n];
	    				
	    				
	    			}
	    			else if (caratteriDisp[n]=='o' || caratteriDisp[n]=='O' ) {
	    				numPosDisp[n]=11;
	    				sommaDisp += numPosDisp[n];
	    				
	    				
	    			}
	    			else if (caratteriDisp[n]=='p' || caratteriDisp[n]=='P') {
	    				numPosDisp[n]=3;
	    				sommaDisp += numPosDisp[n];
	    				
	    				
	    			}
	    			else if (caratteriDisp[n]=='q' || caratteriDisp[n]=='Q') {
	    				numPosDisp[n]=6;
	    				sommaDisp += numPosDisp[n];
	    				
	    				
	    			}
	    			else if (caratteriDisp[n]=='r' || caratteriDisp[n]=='R') {
	    				numPosDisp[n]=8;
	    				sommaDisp += numPosDisp[n];
	    				
	    				
	    			}
	    			else if (caratteriDisp[n]=='s' || caratteriDisp[n]=='S') {
	    				numPosDisp[n]=12;
	    				sommaDisp += numPosDisp[n];
	    				
	    				
	    			}
	    			else if (caratteriDisp[n]=='t' || caratteriDisp[n]=='T') {
	    				numPosDisp[n]=14;
	    				sommaDisp += numPosDisp[n];
	    				
	    				
	    			}
	    			else if (caratteriDisp[n]=='u' || caratteriDisp[n]=='U') {
	    				numPosDisp[n]=16;
	    				sommaDisp += numPosDisp[n];
	    				
	    				
	    			}
	    			else if (caratteriDisp[n]=='v' || caratteriDisp[n]=='V') {
	    				numPosDisp[n]=10;
	    				sommaDisp += numPosDisp[n];
	    				
	    				
	    			}
	    			else if (caratteriDisp[n]=='w' || caratteriDisp[n]=='W') {
	    				numPosDisp[n]=22;
	    				sommaDisp += numPosDisp[n];
	    				
	    				
	    			}
	    			else if (caratteriDisp[n]=='x' || caratteriDisp[n]=='X') {
	    				numPosDisp[n]=25;
	    				sommaDisp += numPosDisp[n];
	    				
	    				
	    			}
	    			else if (caratteriDisp[n]=='y' || caratteriDisp[n]=='Y') {
	    				numPosDisp[n]=24;
	    				sommaDisp += numPosDisp[n];
	    				
	    				
	    			}
	    			else if (caratteriDisp[n]=='z' || caratteriDisp[n]=='Z') {
	    				numPosDisp[n]=23;
	    				sommaDisp += numPosDisp[n];
	    				
	    				
	    			}
	    			
	    }
	    	
}
	    	
		int tot = sommaPari + sommaDisp;
		
//		converto il resto della divisione
		if (tot%26 == 0) {
			w = 'A';
		}
		else if(tot%26 == 1) {
			w = 'B';
		}
		else if(tot%26 == 2) {
			w = 'C';
		}
		else if(tot%26 == 3) {
			w = 'D';
		}
		else if(tot%26 == 4) {
			w = 'E';
		}
		else if(tot%26 == 5) {
			w = 'F';
		}
		else if(tot%26 == 6) {
			w = 'G';
		}
		else if(tot%26 == 7) {
			w = 'H';
		}
		else if(tot%26 == 8) {
			w = 'I';
		}
		else if(tot%26 == 9) {
			w = 'J';
		}
		else if(tot%26 == 10) {
			w = 'K';
		}
		else if(tot%26 == 11) {
			w = 'L';
		}
		else if(tot%26 == 12) {
			w = 'M';
		}
		else if(tot%26 == 13) {
			w = 'N';
		}
		else if(tot%26 == 14) {
			w = 'O';
		}
		else if(tot%26 == 15) {
			w = 'P';
		}
		else if(tot%26 == 16) {
			w = 'Q';
		}
		else if(tot%26 == 17) {
			w = 'R';
		}
		else if(tot%26 == 18) {
			w = 'S';
		}
		else if(tot%26 == 19) {
			w = 'T';
		}
		else if(tot%26 == 20) {
			w = 'U';
		}
		else if(tot%26 == 21) {
			w = 'V';
		}
		else if(tot%26 == 22) {
			w = 'W';
		}
		else if(tot%26 == 23) {
			w = 'X';
		}
		else if(tot%26 == 24) {
			w = 'Y';
		}
		else if(tot%26 == 25) {
			w = 'Z';
		}
		
		
}
	
 String cidA = "" + w ;
 String ok2= "" + CodiceFiscale.charAt(15);
 String cidB = ok2.toUpperCase();
 
  
 /* if (cidA.contains(cidB)) LetteradiControllo = true;
  else LetteradiControllo = false;*/
  
  return cidA.contains(cidB);
}
	
	
	public void schedaUtente() {
		
		//this.immettiCodice(CodiceFiscale);
		
		System.out.println("Ecco la scheda utente:");
		
		System.out.println("Nome:" + this.getNome());
		System.out.println("Cognome:" + this.getCognome());
		
		System.out.println("Data di nascita:");
		this.getDatadiNascita();
		
		this.getCittaNatale();
		this.getGenere();
}
	}
	

