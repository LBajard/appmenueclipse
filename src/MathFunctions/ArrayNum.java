package MathFunctions;

import java.util.Scanner;

import Main.Main;

public class ArrayNum {
 public void Start() {
	 
	 System.out.println("\n**** RIEMPIE UN ARRAY NUMERICO ****");
	 
	 System.out.println("Quanti elementi nell'array? Inserisci un numero>0:");
	 
     try (Scanner input6 = new Scanner(System.in)) {
	 int k = input6.nextInt();
	 int[] arrayNum = new int[k];
    
	
	 for (int h=0; h<k; h++) {
		 System.out.println("Inserisci un intero:");
		 Scanner input7 = new Scanner(System.in);
		 int j = input7.nextInt();

		 arrayNum[h] = j;
	 }
	 
	 String frase = "";
	 for (int h=0; h<k; h++) {
		 if (h<k-1) {
	         frase += arrayNum[h] + "," + " ";
	     }
		 else if (h == k-1) {
			 frase += arrayNum[h];
		 }
     }
	 
	 System.out.println("Ecco l'array:");
	 System.out.println("[" + frase + "]");
	 
     }
	 
	 catch(Exception e) {
		 System.out.println("Non valido.");
	}
     System.out.println("Inserisci 1 per continuare, zero per tornare al men�."); 
	   	Scanner finaliInpuf = new Scanner(System.in);
	       int comandMenu = finaliInpuf.nextInt();

	   	
	   	if(comandMenu == 1) {
	   		Start();
	   	} else {
	   		Main myMain = new Main();
	   		myMain.startMenu();
	   	}
 }
 }

